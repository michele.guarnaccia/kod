package com.yuzo.kod.refiner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yuzo.kod.refiner.RefineService;

import lombok.extern.java.Log;

@RestController
@Log
public class RefinerController {

	@Autowired
	RefineService refiner;
	
	@GetMapping("/refine")
	public String refine() {
		log.info("Start refiner");
		refiner.refineCards();
		return "OK";
	}
	
}
