package com.yuzo.kod.refiner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yuzo.kod.neo4j.Archetype;
import com.yuzo.kod.neo4j.ArchetypeRepository;
import com.yuzo.kod.neo4j.Attribute;
import com.yuzo.kod.neo4j.AttributeeRepository;
import com.yuzo.kod.neo4j.Card;
import com.yuzo.kod.neo4j.CardRepository;
import com.yuzo.kod.neo4j.Race;
import com.yuzo.kod.neo4j.RaceRepository;
import com.yuzo.kod.neo4j.Type;
import com.yuzo.kod.neo4j.TypeRepository;
import com.yuzo.kod.refiner.pattern.TextPattern;
import lombok.extern.java.Log;

@Service
@Log
public class RefineService {

	@Autowired
	CardRepository cardRepo;
	@Autowired
	ArchetypeRepository archRepo;
	@Autowired
	AttributeeRepository attributeRepo;
	@Autowired
	TypeRepository typeRepo;
	@Autowired
	RaceRepository raceRepo;
	@Autowired
	SentenceExtractor extractor;

	Map<String,Archetype> archTypeList;
	Map<String,Type> typeList;
	Map<String,Attribute> attrList;
	Map<String,Race> raceList;
	Map<String,Card> aliasList;

	public RefineService() {
		typeList = new HashMap<>();
		archTypeList = new HashMap<>();
		attrList = new HashMap<>();
		raceList = new HashMap<>();
		aliasList = new HashMap<>();
		attributeRepo.findAll().stream()
			.forEach(at -> attrList.put(at.getName(), at));
	}

	public void refineCards() {
		log.info("Retrieve all cards...");
		List<Card> all = cardRepo.findAll();

		log.info("Process all cards...");

		all.stream()
			.limit(100) //TODO: only for test
			.forEach(card -> {
				this.prepareBasicEntities(card);
				card = extractor.extract(card, cardRepo);
				card = handleAliasName(card);

				this.saveRefinedCard(card);
			}
		);
	}

	private void saveRefinedCard(Card card) {
		try {
				cardRepo.save(card);
		} catch (Exception e) {
			log.severe("Error while saving card: " + card.getName());
		}
	}

	/*
	 * Prepare Card entity, linking all other referenced entities
	 * Attribute
	 * Type
	 * etc.
	 */
	private Card prepareBasicEntities(Card card) {

		String archetype = card.getArchetype();
		if (archetype != null) {
			Archetype a = archTypeList.get(archetype);
			if (a == null) {
				log.info("Saving archetype " + archetype ); 
				a = archRepo.save(new Archetype(archetype));
				archTypeList.put(archetype, a);
			}
			Set<Archetype> archs = 
					card.getRelatedByArchetype() == null 
					? new HashSet<>() 
					: card.getRelatedByArchetype();
			archs.add(a);
			card.setRelatedByArchetype(archs);
		}

		String type = card.getType();
		if (type != null) {	
			Type t = typeList.get(type);
			if (t == null) {
				log.info("Saving type " + type ); 
				t = typeRepo.save(new Type(type));
				typeList.put(type, t);
			}
			
			Set<Type> types = 
					card.getRelatedByType() == null 
					? new HashSet<>() 
					: card.getRelatedByType();
			types.add(t);
			card.setRelatedByType(types);
		}

		String race = card.getRace();
		if (race != null) {	
			Race r = raceList.get(race);
			if (r == null) {
				log.info("Saving race " + race ); 
				r = raceRepo.save(new Race(race));
				raceList.put(race, r);
			}
			
			Set<Race> races = 
					card.getRelatedByRace() == null 
					? new HashSet<>() 
					: card.getRelatedByRace();
			races.add(r);
			card.setRelatedByRace(races);
		}

		String attr = card.getAttribute();
		if (attr != null) {
			Attribute at = attrList.get(attr);
			Set<Attribute> attrs = 
					card.getRelatedByAttribute() == null 
					? new HashSet<>() 
					: card.getRelatedByAttribute();
			attrs.add(at);
			card.setRelatedByAttribute(attrs);
		}

		return card;
	}

	private Card handleAliasName(Card card) {
		for (String pattern : TextPattern.PATTERN_CARD_ALIAS) {
			Card aliasCard = null;
			Pattern cardAlias = Pattern.compile(
					pattern, Pattern.CASE_INSENSITIVE);
			Matcher md1 = cardAlias.matcher(card.getDesc());
			if (md1.find()) {
				String match = md1.group(1);
				aliasCard = aliasList.get(match);

				if (aliasCard == null) {
					aliasCard = cardRepo.findByName(match);

					if (aliasCard == null) 
						continue;

					log.info("New alias card found");
					aliasList.put(match, aliasCard);
				}

				Set<Card> alias = 
						card.getRelatedByAlias() == null 
						? new HashSet<>() 
						: card.getRelatedByAlias();
				alias.add(aliasCard);
				card.setRelatedByAlias(alias);
			}
		}

		return card;
	}

	private Card handleArchetype(Card card) {
		return card;		

	}
}
