package com.yuzo.kod.refiner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import org.springframework.stereotype.Component;
import com.yuzo.kod.neo4j.Card;
import com.yuzo.kod.neo4j.CardRepository;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreQuote;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

@Component
public class SentenceExtractor {

 	public Card extract(Card card, CardRepository cardRepo) {
		Properties props = new Properties();
	    // set the list of annotators to run
	    props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner,quote");
	    props.setProperty("quote.attributeQuotes","false");
	    // build pipeline
	    StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
	    // create a document object
	    CoreDocument doc = new CoreDocument(card.getDesc());
	    // annotate
	    pipeline.annotate(doc);

	    List<String> sentences = new ArrayList<>();
	    for (CoreSentence sent : doc.sentences()) {
	        sentences.add(sent.text());
	    }
	    card.setSentences(sentences);
	    
	    if (doc.quotes() != null) {
		    Set<Card> mentions = new HashSet<>();
		    for (CoreQuote ent : doc.quotes()) {
		    	Card mention = cardRepo.findByName(ent.text());
		    	if (mention != null)
		    		mentions.add(mention);
		    }
		    card.setRelatedByTextMention(mentions);
	    }

		return card;
	}
}
