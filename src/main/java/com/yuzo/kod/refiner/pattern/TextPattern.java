package com.yuzo.kod.refiner.pattern;

public class TextPattern {
	public static String[] PATTERN_CARD_ALIAS = {
			"This card's name is always treated as \"(.+)\"",
			"This card's name becomes \"(.+)\""
	};
	public static String PATTERN_ARCHETYPE_ALIAS = "This card's name is always treated as an? \"(.+)\" card"; 

}
