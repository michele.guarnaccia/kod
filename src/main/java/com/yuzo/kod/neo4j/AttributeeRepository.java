package com.yuzo.kod.neo4j;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface AttributeeRepository extends Neo4jRepository<Attribute, Long> {

	Attribute findByName(String text);
}
