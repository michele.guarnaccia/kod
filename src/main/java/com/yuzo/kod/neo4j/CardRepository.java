package com.yuzo.kod.neo4j;

import java.util.List;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface CardRepository extends Neo4jRepository <Card, Long> {

    //@Query("MATCH (n:Card{name:'{0}' }) return n")
    Card findByName(String text);
    
    List<Card> findByArchetype(Archetype a);
}
