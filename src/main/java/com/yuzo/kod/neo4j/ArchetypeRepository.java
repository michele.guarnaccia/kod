package com.yuzo.kod.neo4j;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface ArchetypeRepository extends Neo4jRepository<Archetype, Long> {

	Archetype findByName(String text);
}
