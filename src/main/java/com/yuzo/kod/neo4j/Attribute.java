package com.yuzo.kod.neo4j;

import org.springframework.data.annotation.Id;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Node;

import lombok.Data;

@Node("Attribute")
@Data
public class Attribute {

	@Id @GeneratedValue
    Long nodeId;
	String name;
	
	public Attribute(String name) {
		super();
		this.name = name;
	}
}
