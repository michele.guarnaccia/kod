package com.yuzo.kod.neo4j;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface TypeRepository extends Neo4jRepository<Type, Long> {

	Type findByName(String text);
}
