package com.yuzo.kod.neo4j;

import org.springframework.data.annotation.Id;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Node;

import lombok.Data;

@Node("Race")
@Data
public class Race {

	@Id @GeneratedValue
    Long nodeId;
	String name;
	
	public Race(String name) {
		super();
		this.name = name;
	}
}
