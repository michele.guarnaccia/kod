package com.yuzo.kod.neo4j;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface RaceRepository extends Neo4jRepository<Race, Long> {

	Race findByName(String text);
}
