package com.yuzo.kod.neo4j;

import java.util.List;
import java.util.Set;

import org.neo4j.ogm.annotation.Relationship;
import org.springframework.data.annotation.Id;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Node;

import lombok.Data;

@Node("Card")
@Data
public class Card {

	@Id @GeneratedValue
    Long nodeId;
	
	//Import
	private Long id;
	private String name;
	private String desc;
	private String type;
	private String race;
	private String archetype;
	private String atk;
	private String def;
	private int level;
	private String attribute;
	
	/* Unused
	private int scale;
	private int linkval;
	private String[] linkmarkers;
	private Object card_sets;
	private Object card_images;
	private Object card_prices;
	*/
	
	//Refinement
	private List<String> sentences;
	
	//ex.: This card's name is always treated as "Umi"
	@Relationship(type = "ALIAS", direction = Relationship.UNDIRECTED)
	public Set<Card> relatedByAlias;
	
	@Relationship(type = "ARCHETYPE", direction = Relationship.UNDIRECTED)
	public Set<Archetype> relatedByArchetype;
	
	@Relationship(type = "MENTION", direction = Relationship.UNDIRECTED)
	public Set<Card> relatedByTextMention;
	
	@Relationship(type = "ATTRIBUTE", direction = Relationship.UNDIRECTED)
	public Set<Attribute> relatedByAttribute;
	
	@Relationship(type = "TYPE", direction = Relationship.UNDIRECTED)
	public Set<Type> relatedByType;
	
	@Relationship(type = "RACE", direction = Relationship.UNDIRECTED)
	public Set<Race> relatedByRace;


}
