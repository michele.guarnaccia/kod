package com.yuzo.kod.neo4j;

import org.springframework.data.annotation.Id;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Node;

import lombok.Data;

@Node("Type")
@Data
public class Type {

	@Id @GeneratedValue
    Long nodeId;
	String name;
	
	public Type(String name) {
		super();
		this.name = name;
	}
}
