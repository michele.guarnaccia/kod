package com.yuzo.kod.neo4j;

import org.springframework.data.annotation.Id;
import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Node;

import lombok.Data;

@Node("Archetype")
@Data
public class Archetype {

	@Id @GeneratedValue
    Long nodeId;
	String name;
	
	public Archetype(String name) {
		this.name = name;
	}
}
