package com.yuzo.kod.importer.model;

import com.yuzo.kod.neo4j.Card;

import lombok.Data;

@Data
public class ImportedCardCollection {

	private Card[] data;
}
