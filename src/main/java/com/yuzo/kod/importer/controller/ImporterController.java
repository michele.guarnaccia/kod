package com.yuzo.kod.importer.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.yuzo.kod.importer.Importer;
import com.yuzo.kod.importer.model.ImportedCardCollection;
import com.yuzo.kod.importer.service.StoreCardsService;

import lombok.extern.java.Log;

@RestController
@Log
public class ImporterController {

	@Autowired 
	StoreCardsService storer;
	
	@GetMapping("/import")
	public String importData() {
		String data = null;
		try {
			log.info("Start importing...");
			data = Importer.readDataFromUrl();
			log.info("Data retrieved! \n\r Parsing data ...");
			ImportedCardCollection ch = Importer.getCards(data);
			log.info(String.format("Retrived %s cards \n\r Saving...", ch.getData().length));
			storer.save(ch);
			log.info(String.format("Saved %s cards", ch.getData().length));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "OK";
	}
}
