package com.yuzo.kod.importer.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yuzo.kod.importer.model.ImportedCardCollection;
import com.yuzo.kod.neo4j.Attribute;
import com.yuzo.kod.neo4j.AttributeeRepository;
import com.yuzo.kod.neo4j.Card;
import com.yuzo.kod.neo4j.CardRepository;

@Service
public class StoreCardsService {

	@Autowired CardRepository repo;
	@Autowired AttributeeRepository attrRepo;

	public void save(ImportedCardCollection ch) {
		saveStandardAttributes();
		Iterable<Card> iterable = Arrays.asList(ch.getData());
		repo.saveAll(iterable);		
	}

	private void saveStandardAttributes() {
		Attribute[] attributes = {
				new Attribute("LIGHT"), 
				new Attribute("DARK"), 
				new Attribute("WIND"), 
				new Attribute("FIRE"), 
				new Attribute("WATER"), 
				new Attribute("EARTH")};
		List<Attribute> al = Arrays.asList(attributes);
		attrRepo.saveAll(al);
	}
}
