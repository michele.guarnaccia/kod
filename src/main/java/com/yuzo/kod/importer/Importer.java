package com.yuzo.kod.importer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.Gson;
import com.yuzo.kod.importer.model.ImportedCardCollection;

import lombok.extern.java.Log;

@Log
public class Importer {

	public static String readDataFromUrl() throws IOException {
		HttpURLConnection c = null;
		String data = null;
		
	    try {
	        URL u = new URL("https://db.ygoprodeck.com/api/v7/cardinfo.php?format=duel%20links");
	        c = (HttpURLConnection) u.openConnection();
	        c.setRequestMethod("GET");
	        c.setUseCaches(false);
	        c.setAllowUserInteraction(false);
	        //there is a filter for non identified requests
	        c.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
	        c.connect();
	        int status = c.getResponseCode();

	        switch (status) {
	            case 200:
	            case 201:
	                BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream()));
	                StringBuilder sb = new StringBuilder();
	                String line;
	                while ((line = br.readLine()) != null) {
	                    sb.append(line+"\n");
	                }
	                br.close();
	                data = sb.toString();
	        }

	    } catch (MalformedURLException ex) {
	        log.severe(ex.getMessage());
	    } catch (IOException ex) {
	        log.severe(ex.getMessage());
	    } finally {
	       if (c != null) {
	          try {
	              c.disconnect();
	          } catch (Exception ex) {
	  	        log.severe(ex.getMessage());
	          }
	       }
	    }	
	    
	    return data;
	}
	
	public static ImportedCardCollection getCards(String data) {
		ImportedCardCollection ch = new Gson().fromJson(data, ImportedCardCollection.class);
		return ch;
	}

}
